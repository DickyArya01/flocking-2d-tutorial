using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmosDrawer : MonoBehaviour
{
    Flock flock;
    List<FlockAgent> agents = new List<FlockAgent>();

    public bool useDebugMode;

    private void Start()
    {
        flock = FindObjectOfType<Flock>();

        if (flock != null)
        {
            agents = flock.Agents;
        }
        else
        {
            Debug.Log("Please insert gameobject with Flock object in it");
        }
    }


    private List<Transform> GetNearbyObject(FlockAgent agent)
    {
        List<Transform> context = new List<Transform>();
        Collider2D[] contextColliders = Physics2D.OverlapCircleAll(agent.transform.position, flock.neighborRadius);

        foreach (Collider2D c in contextColliders)
        {
           if (c != agent.AgentCollider)
           {
                context.Add(c.transform);
           } 
        }
        return context;
    }

    private void DrawLineNeighborToNeighbor()
    {
        foreach (FlockAgent agent in agents)
        {
            List<Transform> context = GetNearbyObject(agent); 

            foreach (Transform neighborAgent in context)
            {
                Gizmos.DrawLine(agent.transform.position, neighborAgent.position);   
            }
        }
    }


    private void OnDrawGizmos()
    {
        if (useDebugMode && flock != null)
        {
            Gizmos.color = Color.yellow;
            DrawLineNeighborToNeighbor();
            
        }

    }

}
