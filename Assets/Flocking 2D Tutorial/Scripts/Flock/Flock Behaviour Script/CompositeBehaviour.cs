using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CompositeBehaviour", menuName = "Flock/Behaviour/Composite", order = 4)]
public class CompositeBehaviour : FlockBehaviour
{
    public FlockBehaviourHandler [] flockBehaviourHandlers;

    public override Vector2 CalculateMove(FlockAgent flockAgent, List<Transform> context, Flock flock)
    {
        // if (flockBehaviourHandlers.Length > 3)
        // {
        //     Debug.Log("Check your FlockBehaviourHandler list "+
        //     "We only need 3 Behaviour : Composite, Alignment, and Avoidance");
        //     return Vector2.zero;
        // }
        return CompositeMove(flockAgent, context, flock);
    }

    public Vector2 CompositeMove(FlockAgent flockAgent, List<Transform> context, Flock flock)
    {
        Vector2 move = Vector2.zero;

        for (int i = 0; i < flockBehaviourHandlers.Length; i++)
        {
            FlockBehaviour behaviour = flockBehaviourHandlers[i].FlockBehaviour;
            float weight = flockBehaviourHandlers[i].FlockBehaviourWeight;
            
            Vector2 partialMove = behaviour.CalculateMove(flockAgent, context, flock) * weight;
            
            if (partialMove !=  Vector2.zero)
            {
                CheckIfOverallMovementExceeedWeight(partialMove, weight);

                move += partialMove;
            }
        }

        return move;
    }

    private void CheckIfOverallMovementExceeedWeight(Vector2 partialMove, float weight)
    {
        if (partialMove.sqrMagnitude > weight * weight)
        {
           partialMove.Normalize();
           partialMove *= weight; 
        }
    }
}
