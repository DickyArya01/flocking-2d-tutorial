using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
    fileName = "AlignmentBehaviour",
    menuName = "Flock/Behaviour/Alignment",
    order = 2
)]
public class AlignmentBehaviour : FilteredFlockBehaviour
{
    public override Vector2 CalculateMove(
        FlockAgent flockAgent,
        List<Transform> context,
        Flock flock
    )
    {
        //if no neighbor, maintain current alignment
        if (context.Count == 0)
            return flockAgent.transform.up;

        //add all points together and average
        Vector2 alignmentMove = Vector2.zero;
        List<Transform> filteredContext = (contextFilter == null) ? context : contextFilter.Filter(flockAgent, context); 
        foreach (Transform item in filteredContext)
        {
            alignmentMove += (Vector2)item.transform.up;
        }

        alignmentMove /= context.Count;

        return alignmentMove;
    }
}
