using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AvoidanceBehaviour",menuName = "Flock/Behaviour/Avoidance", order = 3)]
public class AvoidanceBehaviour : FilteredFlockBehaviour 
{
    public override Vector2 CalculateMove(FlockAgent flockAgent, List<Transform> context, Flock flock)
    {
        //if no neighbor, maintain current alignment
        if (context.Count == 0)
            return Vector2.zero;

        //add all points together and average
        Vector2 avoidanceMove = Vector2.zero;
        int nAvoids = 0;
        List<Transform> filteredContext = (contextFilter == null) ? context : contextFilter.Filter(flockAgent, context); 
        foreach (Transform item in filteredContext)
        {
            if (Vector2.SqrMagnitude(item.position - flockAgent.transform.position) < flock.SquareAvoidanceRadius)
            {
                nAvoids++;
                avoidanceMove += (Vector2)(flockAgent.transform.position - item.position);
            }
        }

        if (nAvoids > 0)
            avoidanceMove /= nAvoids;

        return avoidanceMove;
    }
}
