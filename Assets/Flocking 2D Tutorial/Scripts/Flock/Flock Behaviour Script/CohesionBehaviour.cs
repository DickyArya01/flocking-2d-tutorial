using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CohesionBehaviour", menuName = "Flock/Behaviour/Cohesion", order = 1)]
public class CohesionBehaviour : FilteredFlockBehaviour 
{
    public override Vector2 CalculateMove(
        FlockAgent flockAgent,
        List<Transform> context,
        Flock flock
    )
    {
        //if no neighbor, no adjustment
        if (context.Count == 0)
            return Vector2.zero;

        //add all points together and average
        Vector2 cohesionMove = Vector2.zero;
        List<Transform> filteredContext = (contextFilter == null) ? context : contextFilter.Filter(flockAgent, context); 
        foreach (Transform item in filteredContext)
        {
            cohesionMove += (Vector2)item.position;
        }

        cohesionMove /= context.Count;

        //create offset from agent position
        cohesionMove -= (Vector2)flockAgent.transform.position;

        return cohesionMove;
    }
}
