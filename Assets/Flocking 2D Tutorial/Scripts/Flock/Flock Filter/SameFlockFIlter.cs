using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SameFlockFilter", menuName = "Flock/Filter/Same Flock", order = 5)]
public class SameFlockFIlter : ContextFilter
{
    public override List<Transform> Filter(FlockAgent flockAgent, List<Transform> original)
    {
        List<Transform> filtered = new List<Transform>();

        foreach (Transform item in original)
        {
            FlockAgent itemAgent = item.GetComponent<FlockAgent>();
            if (itemAgent != null && itemAgent.AgentFlock == flockAgent.AgentFlock)
            {
                filtered.Add(item);   
            }   
        }

        return filtered;
    }
}
