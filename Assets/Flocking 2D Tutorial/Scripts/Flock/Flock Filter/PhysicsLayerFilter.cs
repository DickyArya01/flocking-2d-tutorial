using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PhysicsFlockFilter", menuName = "Flock/Filter/Physics Flock", order = 5)]
public class PhysicsLayerFilter : ContextFilter
{
    
    public LayerMask layer;
    public override List<Transform> Filter(FlockAgent flockAgent, List<Transform> original)
    {
        List<Transform> filtered = new List<Transform>();

        foreach (Transform item in original)
        {
            if (layer == (layer | (1 << item.gameObject.layer)))
            {
                filtered.Add(item);   
            }
        }

        return filtered;
    }

}
