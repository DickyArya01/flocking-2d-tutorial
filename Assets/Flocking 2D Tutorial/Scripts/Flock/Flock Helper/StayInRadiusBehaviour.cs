using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StayInRadiusBehaviour", menuName = "Flock/Behaviour/Stay in Radius", order = 5)]
public class StayInRadiusBehaviour : FlockBehaviour
{
    public Vector2 center;
    public float radius;

    public override Vector2 CalculateMove(FlockAgent flockAgent, List<Transform> context, Flock flock)
    {
        Vector2 centerOffset = center - (Vector2) flockAgent.transform.position;
        float t = centerOffset.magnitude / radius;
        if (t < 0.9)
        {
            return Vector2.zero;   
        }

        return centerOffset * t * t;
    }
}
