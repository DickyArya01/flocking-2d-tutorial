using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "BehaviourHandler", menuName = "Flock/Behaviour/BehaviourHandler", order = 5)]
public class FlockBehaviourHandler : ScriptableObject 
{
    [SerializeField]
    private FlockBehaviour flockBehaviour;

    [SerializeField]
    private float flockBehaviourWeight;

    public FlockBehaviour FlockBehaviour{ get {
        return flockBehaviour;
    }}

    public float FlockBehaviourWeight { get {
        return flockBehaviourWeight;
    }}
}
