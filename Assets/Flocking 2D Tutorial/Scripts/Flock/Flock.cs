using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour
{
    [HideInInspector] public List<FlockAgent> Agents { get { return agents; }}

    public FlockAgent agentPrefab;
    List<FlockAgent> agents = new List<FlockAgent>();
    public FlockBehaviour behaviour;

    [Range(10, 500)]
    public int startingCount = 250;
    const float AgentDensity = 0.08f;

    [Range(1f, 100f)]
    public float driveFactor = 10f;
    [Range(1f, 100f)]
    public float maxSpeed = 5f;
    [Range(1f, 10f)]
    public float neighborRadius = 1.5f;
    [Range(0f, 1f)]
    public float avoidanceRadiusMultiplier = 0.5f;

    float squareMaxSpeed;
    float squareNeighborRadius;
    float squareAvoidanceRadius;
    public float SquareAvoidanceRadius { get{ return squareAvoidanceRadius; } }

    public GameObject obstacle;

    void SpawnFlockAgent()
    {
        for (int i = 0; i < startingCount; i++)
        {
            var randomPos = Random.insideUnitCircle * startingCount * AgentDensity;

            FlockAgent newAgent = Instantiate(
                                     agentPrefab, 
                                     randomPos, 
                                     Quaternion.Euler(Vector3.forward * Random.Range(0f, 360f)),
                                     this.transform);
            
            newAgent.name = "Flock Agent " + i;
            newAgent.Initialize(this); 
            agents.Add(newAgent);


        }
    }
    private List<Transform> GetNearbyObject(FlockAgent agent)
    {
        List<Transform> context = new List<Transform>();
        Collider2D[] contextColliders = Physics2D.OverlapCircleAll(agent.transform.position, neighborRadius);

        foreach (Collider2D c in contextColliders)
        {
           if (c != agent.AgentCollider)
           {
                context.Add(c.transform);
           } 
        }
        

        return context;
    }

    private void SetUpBehaviourForEachAgent()
    { 
       foreach (FlockAgent agent in agents)
       { 
            List<Transform> context = GetNearbyObject(agent); 

            Vector2 move = behaviour.CalculateMove(agent, context, this);
            move *= driveFactor;
            if (move.sqrMagnitude > squareMaxSpeed)
            {
                move = move.normalized * maxSpeed;
                agent.Move(move);
            }
       }
    }

    private void Start()
    {
        squareMaxSpeed = maxSpeed * maxSpeed;
        squareNeighborRadius = neighborRadius * neighborRadius;
        squareAvoidanceRadius = squareNeighborRadius * avoidanceRadiusMultiplier ;

        SpawnFlockAgent();

    }

    private void Update()
    {
        SetUpBehaviourForEachAgent();
    }
}
    




















